##Afterpay Touch - Fraud Detection Code Test

The fraud detection implementation has been separated into two distinct concerns:
- The public facing API, which conforms to the requested method specification of

  - List<String\> transactions, where each item represents an serialized transaction
  - LocalDate targetDate, which represents the single day within which we will apply the fraud detection rules on the input
  - Double detectionThreshold, which represents the threshold upon which aggregated transactions for a given group may be considered fraudulent
  
- The package protected detection algorithm which expects

  - List<Transaction> transactions, where each item represents a validated transaction
  - LocalDate targetDate, which represents the single day within which we will apply the fraud detection rules on the input
  - Double detectionThreshold, which represents the threshold upon which aggregated transactions for a given group may be considered fraudulent
    
The Fraud Detector is responsible for transforming serialized input into transaction models and ensuring the correctness of these models.
The Detection Algorithm is responsible for applying the fraud detection rules to a List of Transactions. 

To run the tests which validate both the detector and the algorithm, simply invoke via. gradle.
```
./gradlew test
```

I have chosen to use pre-existing solutions for both input deserialization and validation rather than roll my own for the following reasons:

- As an existing open source implementation, the jackson object mapping library handles many corner cases with badly formed input that may not be obvious to a developer

  - It's also a commonly used library, which should allow other developers to easily comprehend both what it's responsible for and the separation of responsibilities within the implementation
  
- Hibernate provides a simple JSR 310 compliant validation model commonly used in Spring / Dropwizard REST applications, allowing the developer to utilise a rich library of pre-existing validators. 

##Notes
Given an environment with a significant volume of transactions, this would be a sub-optimal solution as it requires all 
transactions to be held within the JVM heap for the detection algorithm to be applied. A cleaner solution would be to utilise 
a time windowed stream processing solution, ie. kafka streams emitting fraudulent credit events to another topic upon a series
of transactions breaching the threshold within a time window.