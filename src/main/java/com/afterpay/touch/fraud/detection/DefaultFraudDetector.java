package com.afterpay.touch.fraud.detection;

import com.afterpay.touch.fraud.model.Transaction;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Default fraud detection implementation. Responsible for deserializing transactions into object models, validating the
 * correctness of these models, then handing off the actual fraud detection to a provided detection algorithm implementation.
 *
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class DefaultFraudDetector implements FraudDetector {
  private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFraudDetector.class);

  private final CsvMapper mapper;
  private final Validator validator;
  private final DetectionAlgorithm detectionAlgorithm;

  @Inject
  DefaultFraudDetector(final CsvMapper mapper, final Validator validator, final DetectionAlgorithm detectionAlgorithm) {
    this.mapper = mapper;
    this.validator = validator;
    this.detectionAlgorithm = detectionAlgorithm;
  }

  @Override
  public List<String> detect(
      final List<String> transactions,
      final LocalDate targetDate,
      final Double detectionThreshold
  ) {
    ObjectReader reader = mapper.readerWithTypedSchemaFor(Transaction.class);
    List<Transaction> validTransactions = Lists.newArrayList();

    // keeping it simple... iterate over each transaction, deserialize into a model, validate and add to the list of
    // transactions to be processed. Badly formed input and invalid models will be ignored
    for(final String transaction: transactions) {
      try {
        final Transaction tx = reader.readValue(transaction);
        final Set<ConstraintViolation<Transaction>> violations = validator.validate(tx);

        if(violations.size() > 0) {
          throw new ConstraintViolationException(violations);
        }

        validTransactions.add(tx);
      } catch(IOException ioe) { // unable to deserialize, generally badly formed input
        LOGGER.warn("Unable to deserialize transaction {}, ignoring", transaction, ioe);
      } catch(ConstraintViolationException cve) { // invalid model content, failed validation
        LOGGER.warn("Invalid transaction detected, discarding", cve);
      }
    }

    // pass all valid transactions to the detection algorithm
    return detectionAlgorithm.detect(validTransactions, targetDate, detectionThreshold);
  }
}
