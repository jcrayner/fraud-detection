package com.afterpay.touch.fraud.detection;

import java.time.LocalDate;
import java.util.List;

/**
 * Interface which describes the public facing fraud detection API.
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public interface FraudDetector {
  List<String> detect(List<String> transactions, LocalDate targetDate, Double fraudThreshold);
}
