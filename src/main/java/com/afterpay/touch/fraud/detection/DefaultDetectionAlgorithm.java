package com.afterpay.touch.fraud.detection;

import com.afterpay.touch.fraud.model.Transaction;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Default fraud detection algorithm. Assumes that all inputs have been validated prior to being executed. Package protected
 * constructor to prevent instantiation outside of the Guice DI pipeline.
 *
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class DefaultDetectionAlgorithm implements DetectionAlgorithm {
  DefaultDetectionAlgorithm() {}

  @Override
  public List<String> detect(final List<Transaction> transactions, final LocalDate targetDate, final Double detectionThreshold) {
    // reduce stream to records within the selected date, aggregate price by cc hash, filter to obtain only records
    // greater than or equal to the detection threshold then return all the matching cc hash keys
    return transactions.stream().filter(transaction -> transaction.getTimestamp().toLocalDate().equals(targetDate))
        .collect(Collectors.groupingBy(Transaction::getCcHash, Collectors.summingDouble(Transaction::getPrice)))
        .entrySet().stream()
        .filter(agg -> agg.getValue() >= detectionThreshold)
        .map(Map.Entry::getKey)
        .collect(Collectors.toList());
  }
}
