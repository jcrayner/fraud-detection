package com.afterpay.touch.fraud.detection;

import com.afterpay.touch.fraud.model.Transaction;

import java.time.LocalDate;
import java.util.List;

/**
 * Detection algorithm interface.
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public interface DetectionAlgorithm {
  List<String> detect(List<Transaction> transactions, LocalDate targetDate, Double detectionThreshold);
}
