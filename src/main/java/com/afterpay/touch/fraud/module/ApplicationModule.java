package com.afterpay.touch.fraud.module;

import com.afterpay.touch.fraud.detection.DefaultDetectionAlgorithm;
import com.afterpay.touch.fraud.detection.DefaultFraudDetector;
import com.afterpay.touch.fraud.detection.DetectionAlgorithm;
import com.afterpay.touch.fraud.detection.FraudDetector;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.hibernate.validator.HibernateValidator;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * Guice configuration module for Dependency Injection.
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class ApplicationModule extends AbstractModule {
    @Provides
    public CsvMapper csvMapper() {
        CsvMapper mapper = new CsvMapper();
        mapper.registerModule(new AfterburnerModule());
        mapper.registerModule(new ParameterNamesModule());
        mapper.registerModules(new Jdk8Module());
        mapper.registerModules(new JavaTimeModule());

        return mapper;
    }

    @Provides
    public ValidatorFactory validatorFactory() {
        return Validation.byProvider(HibernateValidator.class).configure().buildValidatorFactory();
    }

    @Provides
    public Validator validator(ValidatorFactory validatorFactory) {
        return validatorFactory.getValidator();
    }

    @Override
    protected void configure() {
      bind(DetectionAlgorithm.class).to(DefaultDetectionAlgorithm.class);
      bind(FraudDetector.class).to(DefaultFraudDetector.class);
    }
}
