package com.afterpay.touch.fraud.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Simple model representing a purchase transaction.
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
@JsonPropertyOrder(value = {"ccHash", "timestamp", "price"})
public class Transaction {
  @NotEmpty
  private final String ccHash;
  @NotNull
  private final LocalDateTime timestamp;
  @Positive
  private final Double price;

  public Transaction(
      @JsonProperty("ccHash") final String ccHash,
      @JsonProperty("timestamp") final LocalDateTime timestamp,
      @JsonProperty("price") final Double price
  ) {
    this.ccHash = ccHash;
    this.timestamp = timestamp;
    this.price = price;
  }

  public String getCcHash() {
    return ccHash;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public Double getPrice() {
    return price;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transaction that = (Transaction) o;
    return Objects.equals(ccHash, that.ccHash) &&
        Objects.equals(timestamp, that.timestamp) &&
        Objects.equals(price, that.price);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ccHash, timestamp, price);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("ccHash", ccHash)
        .add("timestamp", timestamp)
        .add("price", price)
        .toString();
  }
}
