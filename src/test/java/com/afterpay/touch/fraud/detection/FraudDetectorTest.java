package com.afterpay.touch.fraud.detection;

import com.afterpay.touch.fraud.module.ApplicationModule;
import com.google.common.collect.ImmutableList;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.collection.IsIterableWithSize;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Fraud detection implementation test. Sets up the default detector implementation for testing, then tests potential
 * input scenarios end to end.
 *
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class FraudDetectorTest {
  private static FraudDetector DETECTOR;

  @BeforeClass
  public static void setUp() {
    Injector injector = Guice.createInjector(new ApplicationModule());
    DETECTOR = injector.getInstance(FraudDetector.class);
  }

  @Test
  public void testDetectWithFraudulentTransactions() throws Exception {
    assertThat(
        DETECTOR.detect(
            ImmutableList.of(
                "\"abc\",\"2018-01-01T00:00:01\",5.00",
                "\"abc\",\"2018-01-01T00:00:02\",4.00",
                "\"abc\",\"2018-01-01T00:00:03\",10.00"
            ),
            LocalDate.parse("2018-01-01"),
            10D
        ),
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(1)),
            IsCollectionContaining.hasItems(equalTo("abc"))
        )
    );
  }

  @Test
  public void testDetectWithNoFraudulentTransactions() throws Exception {
    assertThat(
        DETECTOR.detect(
            ImmutableList.of(
                // validates that transactions outside of the date range are not considered
                "\"abc\",\"2018-01-01T00:00:01\",5.00",
                "\"abc\",\"2018-01-01T00:00:02\",4.00",
                "\"abc\",\"2018-01-02T00:00:03\",9.00"
            ),
            LocalDate.parse("2018-01-01"),
            10D
        ),
        IsEmptyCollection.emptyCollectionOf(String.class)
    );
  }

  @Test
  public void testDetectWithBadlyFormedInput() throws Exception {
    assertThat(
        DETECTOR.detect(
            ImmutableList.of(
                "abc,123", // invalid input, not enough fields
                "\"\",\"2018-01-01T00:00:01\",5.00", // invalid model, cc hash may not be empty
                "\"abc\",\"2018-01-01T00:00:02\",-4.00", // invalid model, price must be a positive double
                "\"abc\",\"\",9.00" // invalid model, timestamp must not be null
            ),
            LocalDate.parse("2018-01-01"),
            10D
        ),
        IsEmptyCollection.emptyCollectionOf(String.class)
    );
  }
}